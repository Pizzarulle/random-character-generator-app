export const removeZeroAtStart = (value) => { // if the value contains more than one digit and starts with a zero, the zero is removed
    if (value.length > 1 && value.charAt(0) === "0") {
        value = value.substring(1)
    }
    return value
}
export const randomIntFromInterval = (min, max) => { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
}