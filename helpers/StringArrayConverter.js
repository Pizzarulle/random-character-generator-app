export const stringToArray = (string) => {
    return string.split(',')
}
export const arrayToString = (array) => {
    return (array.toString().replace(/[,]/g, ', ') + ', ')
}

export const cleanString = (string) => {
    let cleanString = string;
    cleanString = cleanString.replace(/\s{2,}/gm, "");
    cleanString = cleanString.replace(/(\r\n|\n|\r)/gm, "")
    cleanString = cleanString.split(',')
    cleanString = cleanString.map(item => item.trim()).filter(item => item.length != 0)
    cleanString = cleanString.toString()
    return cleanString;
}