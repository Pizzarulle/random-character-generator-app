import React from 'react';
import { Provider } from "react-redux";
import { applyMiddleware, combineReducers, createStore } from "redux";
import ReduxThunk from "redux-thunk";
import { initDb } from "./helpers/db";
import Navigator from "./navigation/Navigator";
import reducer from "./store/reducers";


initDb().then(() => {
  console.log("Initialized databade");
}).catch(error => {
  console.log("Initializing db failed");
  console.log(error);
})


const rootReducer = combineReducers({
  lists: reducer
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))

export default function App() {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
}


