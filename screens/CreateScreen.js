import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useDispatch } from 'react-redux';
import Dialog from '../components/Dialog';
import HeaderButton from "../components/HeaderButton";
import { cleanString } from '../helpers/StringArrayConverter';
import { addList } from '../store/actions';

const CreateScreen = (props) => {

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')

    const titleInputHandler = (value) => {
        setTitle(value);
    }
    const contentInputHandler = (value) => {
        setContent(value)
    }

    const dispatch = useDispatch();

    const saveNewList = useCallback(() => {
        dispatch(addList(title, cleanString(content)))
        props.navigation.goBack();

    }, [title, content])

    useEffect(() => {
        props.navigation.setParams({ save: saveNewList })
    }, [saveNewList])

    return (

        <View style={styles.screen}>
            <Dialog />
            <TextInput
                style={styles.input}
                value={title}
                onChangeText={titleInputHandler}
                placeholder="Title"
            />
            <TextInput
                style={styles.textArea}
                value={content}
                onChangeText={contentInputHandler}
                placeholder="Type something"
                numberOfLines={5}
                multiline={true}
            />
        </View>
    );
}

CreateScreen.navigationOptions = navData => {
    return {
        headerTitle: "Create a new category",
        headerRight: () => {
            return (
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item title="Checkmark" iconName="md-checkmark-sharp" onPress={navData.navigation.getParam("save")} />
                </HeaderButtons>
            )
        }
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        margin: 10,
        backgroundColor: "white",
        borderRadius: 10,
        elevation: 3,
        alignItems: 'center'
    },
    input: {
        fontSize: 30,
        borderBottomColor: "black",
        borderBottomWidth: 1,
        marginVertical: 10,
        width: "80%",
        height: 75,
    },
    textArea: {
        flex: 1,
        textAlignVertical: "top",
        width: "80%",
        height: "80%",
        fontSize: 24,
    }
})




export default CreateScreen;