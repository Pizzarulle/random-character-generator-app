import React, { useEffect } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import FloatingButton from "../components/FloatingButton";
import ItemCard from "../components/ItemCard";
import { loadLists } from '../store/actions';
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";



const MenuScreen = (props) => {
    const lists = useSelector(state => state.lists.lists)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadLists());
    }, [dispatch])
    return (
        <View style={styles.container}>
            <FlatList
                data={lists}
                keyExtractor={item => item.id.toString()}
                renderItem={itemData => (
                    <ItemCard
                        title={itemData.item.title}
                        content={itemData.item.content}
                        onAction={() => (
                            props.navigation.navigate("Game", {
                                pid: itemData.item.id,
                                listTitle: itemData.item.title,
                            }))}
                    />
                )}
            />
            <View>
                <FloatingButton style={{ bottom: 20, right: 20 }} onAddActivity={() => props.navigation.navigate("Create")} />
            </View>
        </View>
    );
}

MenuScreen.navigationOptions = navData => {
    return {
        headerTitle: "Random Character Generator",
        headerLeft: () => {
            return (
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item
                        title="Menu"
                        iconName="md-menu"
                        onPress={() => { navData.navigation.toggleDrawer() }}
                    />
                </HeaderButtons>
            )
        }
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },

});

export default MenuScreen;