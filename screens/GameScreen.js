import React, { useState } from 'react';
import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector } from "react-redux";
import HeaderButton from "../components/HeaderButton";


const GameScreen = (props) => {
    const pid = props.navigation.getParam("pid")
    const lists = useSelector(state => state.lists.lists)
    const orgList = lists.find(item => item.id === pid).content;

    const [newList, setNewList] = useState([])
    const [restart, setRestart] = useState(false)

    const next = () => {
        if (!restart) {
            const rndIndex = Math.floor(Math.random() * orgList.length);

            if (newList.length !== orgList.length) {
                if (newList.filter(item => item.key === rndIndex).length === 0) {
                    let rndObject = {
                        key: rndIndex,
                        value: orgList[rndIndex]
                    }
                    setNewList(currentList => [...currentList, rndObject])
                }
                else {
                    next()
                }
            }

            if (newList.length === orgList.length) {
                setRestart(true)
            }

        } else {
            setNewList([])
            setRestart(false)
        }
    }

    let gameText;

    if (newList.length === 0) {
        gameText = <Text style={{ fontSize: 24 }}>Tap to begin!</Text>
    }
    else {
        gameText = <Text style={{ fontSize: 200 }} adjustsFontSizeToFit minimumFontScale={0.05} numberOfLines={1}>{newList.length > 0 && newList[newList.length - 1].value.toUpperCase()}</Text>
    }
    if (restart) {
        gameText = <Text style={{ fontSize: 24 }}>Tap again to restart!</Text>
    }

    return (
        <View style={styles.container}>
            <View style={styles.prevCharContainer}>
                <View style={{ maxHeight: 30 }}>
                    <Text style={{ fontSize: 30 }} adjustsFontSizeToFit minimumFontScale={.001}>
                        {newList.map((item, index) => (
                            index !== newList.length - 1 ?
                                item.value.toUpperCase() + ", "
                                :
                                item.value.toUpperCase()
                        ))}
                    </Text>
                </View>
                <Text style={{ fontSize: 23 }}>{newList.length} av {orgList.length}</Text>
            </View>
            <TouchableWithoutFeedback onPress={next}>
                <View style={styles.charContainer}>
                    {gameText}
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

GameScreen.navigationOptions = navData => {
    return {
        headerTitle: navData.navigation.getParam("listTitle"),
        headerRight: () => {
            return (<HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Settings"
                    iconName="md-settings-sharp"
                    onPress={() => navData.navigation.navigate("Edit", {
                        pid: navData.navigation.getParam("pid"),
                    })} />
            </HeaderButtons>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    prevCharContainer: {
        padding: 10,
        height: 70,
        justifyContent: "flex-end"
    },
    charContainer: {
        flex: 1,
        marginHorizontal: 10,
        marginBottom: 10,
        borderRadius: 10,
        paddingHorizontal: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        elevation: 3
    },


});
export default GameScreen;