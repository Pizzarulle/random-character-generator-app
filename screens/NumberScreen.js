import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

import Colour from '../constants/Colour';
import SimpleNumberView from "../components/SimpleNumberView";
import AdvancedNumberView from "../components/AdvancedNumberView";


const NumberScreen = () => {
    const [screenContent, setScreenContent] = useState(true);
    const screenContentHandler = (status) => {
        if (screenContent !== status) {
            setScreenContent(status)
        }
    }
    return (
        <View style={styles.screen}>
            <View style={styles.tabBarRow}>
                <TouchableOpacity style={styles.button} onPress={() => screenContentHandler(false)}>
                    <Text style={styles.optionText}>Simple</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => screenContentHandler(true)}>
                    <Text style={styles.optionText}>Advanced</Text>
                </TouchableOpacity>
            </View>
            {
                screenContent ? <AdvancedNumberView /> : <SimpleNumberView />
            }

        </View>
    );
}

NumberScreen.navigationOptions = navData => {
    return {
        headerTitle: "Random Number Generator",
        headerLeft: () => {
            return (
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item
                        title="Menu"
                        iconName="md-menu"
                        onPress={() => { navData.navigation.toggleDrawer() }}
                    />
                </HeaderButtons>
            )
        }
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10
    },

    tabBarRow: {
        flexDirection: "row",
        justifyContent: "space-around",
        position: 'relative'
    },
    button: {
        backgroundColor: Colour.primary,
        borderRadius: 10,
        paddingHorizontal: 30,
        paddingVertical: 7,
        elevation: 4
    },
    optionText: {
        fontWeight: "bold",
        color: "white",
        fontSize: 18
    }
})
export default NumberScreen;