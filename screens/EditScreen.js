import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useDispatch, useSelector } from "react-redux";
import HeaderButton from "../components/HeaderButton";
import { arrayToString, cleanString } from '../helpers/StringArrayConverter';
import { deleteCurrentList, updateCurrentList } from "../store/actions";


const EditScreen = (props) => {
    const pid = props.navigation.getParam("pid")
    const item = useSelector(state => state.lists.lists).filter(item => item.id === pid)[0]
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')

    const titleInputHandler = (value) => {
        setTitle(value);
    }
    const contentInputHandler = (value) => {
        setContent(value)
    }

    useEffect(() => {
        if (item) {
            setTitle(item.title)
            setContent(arrayToString(item.content))
        }
    }, [item])
    const dispatch = useDispatch();

    const updateCurrentListHandler = useCallback(() => {
        dispatch(updateCurrentList(pid, title, cleanString(content)))

        props.navigation.navigate("Menu")
    }, [pid, title, content])


    const deleteCurrentListHandler = useCallback(() => {
        dispatch(deleteCurrentList(pid))
        props.navigation.navigate("Menu")
    }, [pid])

    useEffect(() => {
        props.navigation.setParams({ delete: deleteCurrentListHandler, update: updateCurrentListHandler })
    }, [updateCurrentListHandler, deleteCurrentListHandler])

    return (
        <View style={styles.screen}>
            <TextInput
                style={styles.input}
                value={title}
                onChangeText={titleInputHandler}
                placeholder="Title"
            />
            <TextInput
                style={styles.textArea}
                value={content}
                onChangeText={contentInputHandler}
                placeholder="Type something"
                numberOfLines={5}
                multiline={true}
            />
        </View>
    );
}

EditScreen.navigationOptions = navData => {
    return {
        headerRight: () => {
            return (
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item title="Trash" iconName="md-trash-sharp" onPress={navData.navigation.getParam("delete")} />
                    <Item title="Checkmark" iconName="md-checkmark-sharp" onPress={navData.navigation.getParam("update")} />
                </HeaderButtons>
            )
        }
    }
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        margin: 10,
        backgroundColor: "white",
        borderRadius: 10,
        elevation: 3,
        alignItems: 'center'
    },
    input: {
        fontSize: 30,
        borderBottomColor: "black",
        borderBottomWidth: 1,
        marginVertical: 10,
        width: "80%",
        height: 75,
    },
    textArea: {
        flex: 1,
        textAlignVertical: "top",
        width: "80%",
        height: "80%",
        fontSize: 24,
    }
})


export default EditScreen;