class ListItem {
    constructor(id, title, content) {
        this.id = id;
        this.title = title;
        this.content = content
    }
}

export default ListItem