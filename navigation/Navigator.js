import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';


import CreateScreen from "../screens/CreateScreen";
import EditScreen from "../screens/EditScreen";
import GameScreen from '../screens/GameScreen';
import MenuScreen from '../screens/MenuScreen';
import NumberScreen from "../screens/NumberScreen";

import Colour from "../constants/Colour"

const CustomListNavigatior = createStackNavigator({
  Menu: MenuScreen,
  Game: GameScreen,
  Edit: EditScreen,
  Create: CreateScreen
},
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Colour.primary
      },
      headerTintColor: Colour.white
    }
  }
);
const NumberNavigator = createStackNavigator({
  Numbers: NumberScreen
},
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: Colour.primary
      },
      headerTintColor: Colour.white
    }
  })

const AppNavigator = createDrawerNavigator(
  {
    Numbers: NumberNavigator,
    Custom: CustomListNavigatior,
  },
  {
    contentOptions: {
      activeTintColor: Colour.primary
    }
  }
)

export default createAppContainer(AppNavigator)