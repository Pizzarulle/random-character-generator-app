import ListItem from "../models/ListItem";
import { ADD_LIST, DELETE_LIST, LOAD_LISTS, UPDATE_LIST } from "./actions";

const initialState = {
    lists: []
}
export default (state = initialState, action) => {
    switch (action.type) {
        case LOAD_LISTS:
            return {
                lists: action.lists.map(item => new ListItem(item.id, item.title, item.listContent.split(',')))
            }
        case ADD_LIST:
            const newListItem = new ListItem(action.itemData.id, action.itemData.title, action.itemData.content.split(','))
            return {
                lists: state.lists.concat(newListItem)
            }
        case UPDATE_LIST:

            const index = state.lists.findIndex(item => item.id === action.itemData.id)
            const updatedListItem = new ListItem(action.itemData.id, action.itemData.title, action.itemData.content.split(','))

            let updatedList = [...state.lists]
            updatedList[index] = updatedListItem
            return {
                lists: updatedList
            }
        case DELETE_LIST:
            return {
                lists: state.lists.filter(item => item.id != action.id)
            }
        default:
            return state

    }
}