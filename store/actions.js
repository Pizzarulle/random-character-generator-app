import { deleteList, fetchLists, insertList, updateList } from "../helpers/db";

export const LOAD_LISTS = "LOAD_LISTS";
export const ADD_LIST = "ADD_LIST";
export const UPDATE_LIST = "UPDATE_LIST";
export const DELETE_LIST = "DELETE_LIST";

export const loadLists = () => {
    return async dispatch => {
        try {
            const dbResult = await fetchLists();
            dispatch({ type: LOAD_LISTS, lists: dbResult.rows._array })
        } catch (error) {
            console.log(error);
            throw error
        }
    }
}
export const addList = (title, content) => {
    return async dispatch => {
        try {
            const dbResult = await insertList(title.toString(), content.toString());
            dispatch({ type: ADD_LIST, itemData: { id: dbResult.insertId, title: title, content: content } })

        } catch (error) {
            console.log(error);
            throw error
        }
    }
}
export const updateCurrentList = (id, newTitle, newContent) => {
    return async dispatch => {
        try {
            const dbResult = await updateList(id, newTitle.toString(), newContent.toString());
            dispatch({ type: UPDATE_LIST, itemData: { id: id, title: newTitle, content: newContent } })
        } catch (error) {
            console.log(error);
            throw console.error();
        }
    }
}
export const deleteCurrentList = (id) => {
    return async dispatch => {
        try {
            const dbresult = await deleteList(id)
            dispatch({ type: DELETE_LIST, id: id })
        } catch (error) {
            console.log(error);
            throw error
        }
    }
}

