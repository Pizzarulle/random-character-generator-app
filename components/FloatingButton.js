import { AntDesign } from "@expo/vector-icons";
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import Colour from "../constants/Colour"

const FloatingButton = (props) => {
    return (
        <TouchableOpacity style={{ ...styles.button, ...props.style }} onPress={props.onAddActivity}>
            <AntDesign name="plus" size={30} color="white" />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        position: "absolute",
    },
    button: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        position: 'absolute',
        height: 80,
        backgroundColor: Colour.primary,
        borderRadius: 100,
    }
})

export default FloatingButton;