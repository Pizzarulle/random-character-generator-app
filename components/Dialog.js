import React, { useState } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Modal, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Colour from '../constants/Colour';

const Dialog = () => {
  const [showDialog, setShowDialog] = useState(false)
  const showDialogHandler = () => {
    setShowDialog(!showDialog)
  }

  return (
    <View style={styles.mainContainer}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={showDialog}
        onRequestClose={showDialogHandler}

      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Under "Title" enter a list of items, separated by a comma ( , )</Text>
            <Text style={styles.modalText}>Ex: Hotdog, Hamburger, Fish And Chips, Pizza</Text>
            <View style={styles.closeButton}>
              <TouchableWithoutFeedback onPress={showDialogHandler}>
                <Text style={styles.textStyle}>Close</Text>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      </Modal>

      <TouchableWithoutFeedback onPress={showDialogHandler}>
        <Ionicons name="help-circle" size={24} color="grey" />
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    position: "absolute",
    top: 10,
    right: 10
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "grey",
    opacity: 0.9,
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    width: 220,
    height: 200
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  closeButton: {
    overflow: "hidden",
    backgroundColor: Colour.primary,
    borderRadius: 20,
    padding: 10,
    width: 100,
    elevation: 2
  }

})

export default Dialog;