import React from 'react';
import { StyleSheet, Text, TouchableNativeFeedback, View } from 'react-native';

const ItemCard = (props) => {
    return (
        <View style={{ overflow: "hidden", borderRadius: 10 }} >
            <TouchableNativeFeedback onPress={props.onAction}>
                <View style={styles.container}>
                    <Text style={styles.title}>{props.title}</Text>
                    <View style={styles.infoContainer}>
                        <Text style={styles.nrOfChar}>{props.content.filter(item => item.length != 0).length}</Text>
                        <Text style={styles.preview} numberOfLines={1}>
                            {props.content.map((item, index) => (
                                index !== props.content.length - 1 ?
                                    item.toUpperCase() + ", "
                                    :
                                    item.toUpperCase()
                            ))}</Text>
                    </View>
                </View>
            </TouchableNativeFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        borderRadius: 10,
        height: 100,
        padding: 10,
        elevation: 3,
        marginVertical: 3,
        overflow: "hidden",
    },
    title: {
        fontSize: 28,
        fontWeight: 'bold',
        height: "60%"
    },
    infoContainer: {
        height: "40%",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    nrOfChar: {
        fontWeight: "bold",
        minWidth: 20,
        flexShrink: 1
    },
    preview: {
        textAlign: "right",
        flexShrink: 1
    }
})

export default ItemCard;