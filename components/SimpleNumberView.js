import { FontAwesome5 } from '@expo/vector-icons';
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, TouchableNativeFeedback, TouchableWithoutFeedback, View } from 'react-native';
import { removeZeroAtStart, randomIntFromInterval } from "../helpers/NumberGenerator"

const SimpleNumberView = () => {
    const [minValue, setMinValue] = useState("0");
    const [maxValue, setMaxValue] = useState("10");
    const [currentNumber, setCurrentNumber] = useState();

    const minValueHandler = (value) => {
        setMinValue(removeZeroAtStart(value));
    }
    const maxValueHandler = (value) => {
        setMaxValue(removeZeroAtStart(value));
    }
    const next = () => {
        if (minValue > maxValue) {
            alert("Minimum value must be lower than the maximum value")
            return
        }
        if (minValue.length === 0 || maxValue.length === 0) {
            alert("You need to enter a min value and a max value")
            return
        }
        const newNumber = randomIntFromInterval(parseInt(minValue), parseInt(maxValue))
        setCurrentNumber(newNumber)
    }

    const reset = () => {
        setCurrentNumber()
    }

    let gameText

    if (typeof currentNumber !== "number") {
        gameText = <Text style={{ fontSize: 24 }}>Tap to begin!</Text>
    }
    else {
        gameText = <Text style={{ fontSize: 200 }} adjustsFontSizeToFit minimumFontScale={0.05} numberOfLines={1}>{currentNumber}</Text>
    }

    return (
        <View style={styles.container}>
            <View style={styles.inputContainer}>
                <View style={styles.inputCard}>
                    <Text style={styles.text}>Min</Text>
                    <TextInput
                        style={styles.textInput}
                        value={minValue}
                        onChangeText={minValueHandler}
                        keyboardType={"numeric"}
                    />
                </View>
                <View style={styles.inputCard}>
                    <Text style={styles.text}>Max</Text>
                    <TextInput
                        style={styles.textInput}
                        value={maxValue}
                        onChangeText={maxValueHandler}
                        keyboardType={"numeric"}
                    />
                </View>
                <TouchableNativeFeedback onPress={reset}>
                    <View style={styles.inputCard}>
                        <Text style={styles.text}>Reset</Text>
                        <View style={{ justifyContent: "center", alignItems: "center", paddingTop: 5 }}>
                            <FontAwesome5 name="redo" size={24} color="black" />
                        </View>
                    </View>
                </TouchableNativeFeedback>
            </View>

            <TouchableWithoutFeedback onPress={next} style={styles.touchArea}>
                <View style={styles.charContainer}>
                    {gameText}
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    charContainer: {
        flex: 1,
        marginBottom: 10,
        borderRadius: 10,
        paddingHorizontal: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        elevation: 3
    },
    inputContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    inputCard: {
        backgroundColor: "white",
        borderRadius: 10,
        elevation: 3,
        marginVertical: 3,
        width: 100,
        height: 100,
        justifyContent: "center",
        alignItems: "center"
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: "black",
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center"
    },
    text: {
        fontSize: 24,
        fontWeight: "bold"
    },
})

export default SimpleNumberView;