import React, { useState } from 'react';
import { StyleSheet, Switch, Text, TextInput, View, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import { removeZeroAtStart, randomIntFromInterval } from "../helpers/NumberGenerator"

const AdvancedNumberView = () => {
    const [minValue, setMinValue] = useState("0")
    const [maxValue, setMaxValue] = useState("10")
    const [quantity, setQuantity] = useState("1")
    const [duplication, setDuplication] = useState(false)
    const [excludeValues, setExcludeValues] = useState("")
    const [currentValues, setCurrentValues] = useState([]);

    const minValueHandler = (value) => {
        setQuantity("1")
        setMinValue(removeZeroAtStart(value));
    }
    const maxValueHandler = (value) => {
        setQuantity("1")
        setMaxValue(removeZeroAtStart(value));
    }
    const quantityHandler = (value) => {
        if (parseInt(value) < 1) {
            value = "1"
        }
        if (!duplication) {
            if (parseInt(value) > getMaxQuantity()) {
                alert("number is exeeding the non dublicate maximun")
                value = getMaxQuantity().toString()
            }
        }
        setQuantity(value)
    }
    const duplicationHandler = () => {
        setDuplication(!duplication)
    }
    const excludeValuesHandler = (value) => {
        setExcludeValues(value.replace(/[^0-9,-]/gm, ''),)
    }
    const getMaxQuantity = () => {
        return parseInt(maxValue) - parseInt(minValue) + 1
    }
    const reset = () => {
        setCurrentValues([])
        setExcludeValues("")
    }

    const next = () => {
        setCurrentValues([])
        quantity.length < 1 && setQuantity("1")

        if (minValue > maxValue) {
            alert("Minimum value must be lower than the maximum value")
            return
        }
        if (minValue.length === 0 || maxValue.length === 0) {
            alert("You need to enter a min value and a max value")
            return
        }

        if (!splitExcludeValues()) {
            return
        }
        generateRandomNumber(splitExcludeValues())
    }

    const generateRandomNumber = (excludeArray) => {
        let tempArray = []
        while (tempArray.length < quantity) {
            let rndValue = randomIntFromInterval(parseInt(minValue), parseInt(maxValue));

            if (excludeArray.length !== 0) {
                if (excludeArray.includes(rndValue)) {
                    continue
                }
            }
            if (duplication) {
                tempArray.push(rndValue)
            }

            if (!duplication) {
                if (!tempArray.includes(rndValue)) {
                    tempArray.push(rndValue)
                }
            }
        }
        setCurrentValues(currentArray => [...currentArray, ...tempArray])
    }

    const splitExcludeValues = () => {
        const excludeValuesArray = excludeValues.split(',').filter(item => item.length > 0).map(item => parseInt(item));
        const filteredValuesArray = excludeValuesArray.filter(i => i >= parseInt(minValue) && i <= parseInt(maxValue))

        let errorMessage;

        if (duplication) {
            if (filteredValuesArray.length < getMaxQuantity()) {
                return filteredValuesArray
            }
            else {
                errorMessage = "You have excluded too many numbers between " + minValue + " and " + maxValue + ".";
            }
        }
        else {
            if (filteredValuesArray.length <= getMaxQuantity() - parseInt(quantity)) {
                return filteredValuesArray
            }
            else {
                errorMessage = "You have excluded too many numbers. If you want " + quantity + " unique numbers between "
                    + minValue + " and " + maxValue;
                errorMessage += (getMaxQuantity() - parseInt(quantity) === 0) ? " you cant exclude and values." : " you can only exclude " + (getMaxQuantity() - parseInt(quantity)).toString()
                    + " values."
            }
        }
        alert(errorMessage)
        return false
    }

    let gameText = "Tap to begin!"

    if (currentValues.length !== 0) {
        gameText = currentValues.map((value, index) => index !== currentValues.length - 1 ? value + ", " : value)
    }

    return (
        <View style={styles.container}>
            <View style={styles.firstRowContainer}>
                <View style={styles.inputContainer}>
                    <Text style={styles.text}>Min</Text>
                    <TextInput
                        style={styles.textInput}
                        value={minValue}
                        onChangeText={minValueHandler}
                        keyboardType={"numeric"}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.text}>Max</Text>
                    <TextInput
                        style={styles.textInput}
                        value={maxValue}
                        onChangeText={maxValueHandler}
                        keyboardType={"numeric"}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.text}>Quanity</Text>
                    <TextInput
                        style={styles.textInput}
                        value={quantity}
                        onChangeText={quantityHandler}
                        keyboardType={"numeric"}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Text style={styles.text}>Rep #</Text>
                    <Switch
                        value={duplication}
                        onValueChange={duplicationHandler}
                    />
                </View>
            </View>

            <View style={styles.secondRowContainer}>
                <View style={styles.excludeValueContainer}>
                    <View >
                        <Text style={styles.text}>Exclude these numbers</Text>
                        <TextInput
                            placeholder={"No numbers entered"}
                            value={excludeValues}
                            onChangeText={excludeValuesHandler}
                            keyboardType={"numeric"}
                            numberOfLines={3}
                            multiline={true}
                            style={styles.excludeValueInput}
                        />
                    </View>
                </View>

                <TouchableNativeFeedback onPress={reset}>
                    <View style={styles.resetContainer}>
                        <Text style={styles.text}>Reset</Text>
                        <View style={{ justifyContent: "center", alignItems: "center", paddingTop: 10 }}>
                            <FontAwesome5 name="redo" size={24} color="black" />
                        </View>
                    </View>
                </TouchableNativeFeedback>
            </View>

            <TouchableWithoutFeedback onPress={next} style={styles.touchArea}>
                <View style={styles.charContainer}>
                    <Text style={{ fontSize: 200 }} adjustsFontSizeToFit minimumFontScale={0.05} numberOfLines={1}>{gameText}</Text>
                </View>
            </TouchableWithoutFeedback>
        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    firstRowContainer: {
        elevation: 3,
        backgroundColor: "white",
        flexDirection: "row",
        borderRadius: 10,
        marginVertical: 5,
        justifyContent: "space-around",
        paddingVertical: 10
    },
    secondRowContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 5
    },
    excludeValueContainer: {
        backgroundColor: "white",
        elevation: 3,
        borderRadius: 10,
        padding: 10,
        width: "70%"
    },
    excludeValueInput: {
        fontSize: 16,
        fontWeight: "bold",
        textAlignVertical: "top",
        maxWidth: 170,
        maxHeight: 80,
        marginTop: 5
    },
    resetContainer: {
        backgroundColor: "white",
        elevation: 3,
        borderRadius: 10,
        padding: 10,
        width: 100,
        alignItems: "center"
    },
    charContainer: {
        flex: 1,
        backgroundColor: "white",
        borderRadius: 10,
        elevation: 3,
        padding: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    inputContainer: {
        maxWidth: "20%",
        width: "20%",
        textAlign: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: "black",
        fontSize: 18,
        fontWeight: "bold",
        textAlign: "center"
    },
    text: {
        fontSize: 20,
        fontWeight: "bold"
    }
})

export default AdvancedNumberView;